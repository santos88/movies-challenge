//
//  FavoriteTVShowManager.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class FavoriteManager: ObservableObject {
    static let shared = FavoriteManager()
    
    func addNewFavoriteTVShow(tvShow: SimpleTvShow) {
        var favorites = getFavoriteTVShows()
        favorites.append(tvShow)
        let data = try! JSONEncoder().encode(favorites)
        UserDefaults.standard.set(data, forKey: "favoritesTVShow")
    }

    func getFavoriteTVShows() -> [SimpleTvShow] {
        guard let data = UserDefaults.standard.data(forKey: "favoritesTVShow") else {
            return []
        }
        let favorites = try! JSONDecoder().decode([SimpleTvShow].self, from: data)
        return favorites
    }

    func removeAllFavoritesTVShows() {
        UserDefaults.standard.removeObject(forKey: "favoritesTVShow")
    }
    
    func removeFromFavoritesTVShows(tvShow: SimpleTvShow) {
        var favorites = getFavoriteTVShows()

        guard let index = favorites.firstIndex(where: { $0.id == tvShow.id }) else {
            return
        }
        favorites.remove(at: index)

        let data = try! JSONEncoder().encode(favorites)
        UserDefaults.standard.set(data, forKey: "favoritesTVShow")
    }
}
