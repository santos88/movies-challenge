//
//  TvShowModel.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

struct TvShowSearched: Codable, Identifiable {
    let score: Double
    let show: SimpleTvShow
    let id: Int

    enum CodingKeys: String, CodingKey {
        case score
        case show
    }
    
    private static var currentId = 0
    private static var idGenerator: Int {
        currentId += 1
        return currentId
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        score = try container.decode(Double.self, forKey: .score)
        show = try container.decode(SimpleTvShow.self, forKey: .show)
        id = TvShowSearched.idGenerator
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: TvShowSearched, rhs: TvShowSearched) -> Bool {
        return lhs.id == rhs.id
    }
}

struct SimpleTvShow: Codable, Identifiable {
    let id: Int
    let url: String
    let name: String
    let image: Poster?
    
    init(tvShow: TvShow) {
        self.id = tvShow.id
        self.url = tvShow.url
        self.name = tvShow.name
        self.image = tvShow.image
    }
}

struct TvShow: Codable {
    let id: Int
    let url: String
    let name: String
    let genres: [String]
    let runtime, averageRuntime: Int?
    let premiered: String
    let ended: String?
    let schedule: Schedule
    let image: Poster?
    let summary: String
    let embedded: Embedded?
    
    enum CodingKeys: String, CodingKey {
        case id, url, name, genres, runtime, averageRuntime, premiered, ended, schedule, image, summary
        case embedded = "_embedded"
    }
}

struct Embedded: Codable {
    let episodes: [Episode]
}

struct Episode: Codable, Identifiable {
    let id: Int
    let name: String
    let season, number: Int
    let image: Poster?
    let summary: String?
    let links: Links

    enum CodingKeys: String, CodingKey {
        case id, name, season, number, image, summary
        case links = "_links"
    }
}

struct Links: Codable {
    let linksSelf: SelfClass

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
    }
}

struct SelfClass: Codable {
    let href: String
}

struct Poster: Codable {
    let medium: String
}

struct Schedule: Codable {
    let time: String
    let days: [String]
}
