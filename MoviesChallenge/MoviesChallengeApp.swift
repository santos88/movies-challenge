//
//  MoviesChallengeApp.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

@main
struct MoviesChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .preferredColorScheme(.dark)
        }
    }
}
