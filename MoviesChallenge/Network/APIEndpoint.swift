//
//  APIEndpoint.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

enum APIEndpoint {
    case searchShows
    case allShows
    case detailShow

    static var baseURL: URL {
        return URL(string: "https://api.tvmaze.com")!
    }

    var url: URL {
        switch self {
        case .searchShows:
            return APIEndpoint.baseURL.appendingPathComponent("/search/shows")
        case .allShows:
            return APIEndpoint.baseURL.appendingPathComponent("/shows")
        case .detailShow:
            return APIEndpoint.baseURL.appendingPathComponent("/shows/")
        }
    }
}
