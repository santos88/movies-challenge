//
//  MovieDetailAPI.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class MovieDetailAPI {
    
    func movieDetail(id: Int, result: @escaping (Result<TvShow, Error>) -> Void) {
        var urlComp = URLComponents(url: APIEndpoint.detailShow.url.appendingPathComponent("\(id)"), resolvingAgainstBaseURL: true)!
        urlComp.queryItems = [URLQueryItem(name: "embed", value: "episodes")]

        guard let url = urlComp.url else {
            result(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid URL"])))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in

            if let error = error {
                result(.failure(error))
                return
            }

            guard let data = data else {
                result(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "No data found"])))
                return
            }

            let decoder = JSONDecoder()
            do {
                let tvShow = try decoder.decode(TvShow.self, from: data)
                result(.success(tvShow))
            } catch {
                result(.failure(error))
            }
        }
        task.resume()
    }

}
