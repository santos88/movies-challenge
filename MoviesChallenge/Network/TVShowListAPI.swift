//
//  TVShowListAPI.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class TVShowListAPI {
    
    func fetchTVShowList(page: Int, completion: @escaping (Result<[SimpleTvShow], Error>) -> Void) {
        var urlComp = URLComponents(url: APIEndpoint.allShows.url, resolvingAgainstBaseURL: true)!
        urlComp.queryItems = [URLQueryItem(name: "page", value: "\(page)")]
        
        guard let url = urlComp.url else {
            completion(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid URL"])))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "No data found"])))
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let tvShowList = try decoder.decode([SimpleTvShow].self, from: data)
                completion(.success(tvShowList))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
}

