//
//  TVShowSearchAPI.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class TVShowSearchAPI {
    
    func searchTVShows(query: String, completion: @escaping (Result<[TvShowSearched], Error>) -> Void) {
        let queryFormatted = query.trimmingCharacters(in: .whitespacesAndNewlines)
                                    .replacingOccurrences(of: " ", with: "&")
        
        var urlComp = URLComponents(url: APIEndpoint.searchShows.url, resolvingAgainstBaseURL: true)!
        urlComp.queryItems = [URLQueryItem(name: "q", value: queryFormatted)]
        
        guard let url = urlComp.url else {
            completion(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid URL"])))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "No data found"])))
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let searchResults = try decoder.decode([TvShowSearched].self, from: data)
                completion(.success(searchResults))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
}

