//
//  EpisodeDetailView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct EpisodeDetailView: View {
    @State var episode: Episode
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                ZStack(alignment: .bottomTrailing) {
                    if let image = episode.image {
                        RemoteImage(urlString: image.medium)
                            .frame(maxWidth: .infinity)
                            .aspectRatio(contentMode: .fill)
                            .clipped()
                    }
                    
                    Text("S\(episode.season) E\(episode.number)")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .padding(5)
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .clipShape(RoundedRectangle(cornerRadius: 4))
                        .padding(8)
                }
                
                SummaryView(summary: episode.summary ?? "No summary found")
                    .padding(.bottom, 24)
                    .padding()
            }
            .frame(maxWidth: .infinity)
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text(episode.name)
                    .font(.headline)
            }
        }
    }
}

#Preview {
    EpisodeDetailView(episode: Episode(id: 1, name: "Episode1", season: 1, number: 1, image: Poster(medium: ""), summary: "Summary", links: Links(linksSelf: SelfClass(href: ""))))
}
