//
//  EpisodeListView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct EpisodeListView: View {
    @EnvironmentObject private var tvShowModel: MovieDetailViewModel
    @State var episodeList: [Episode] = []
    @Binding var selectedSeason: Int
    private let imageDimention: CGFloat = {
        return UIScreen.main.bounds.width / 3
    }()
    
    private var columns: [GridItem] {
        let numberOfColumns = 3
        let spacing: CGFloat = 10
        let totalPadding: CGFloat = 20
        let availableWidth = UIScreen.main.bounds.width - (spacing * CGFloat(numberOfColumns - 1)) - totalPadding
        let itemWidth = availableWidth / CGFloat(numberOfColumns)
        
        return Array(repeating: GridItem(.fixed(itemWidth), spacing: spacing), count: numberOfColumns)
    }
    
    var body: some View {
        LazyVGrid(columns: Array(repeating: GridItem(.flexible()), count: 3), spacing: 2) {
            ForEach(episodeList) { episode in
                NavigationLink {
                    EpisodeDetailView(episode: episode)
                } label: {
                    episodeCell(for: episode)
                }
                .padding(.bottom, 8)
            }
        }
        .onChange(of: selectedSeason, { _, _ in
            loadEpisodes()
        })
        .onAppear(perform: loadEpisodes)
    }
    
    func loadEpisodes() {
        episodeList = tvShowModel.getEpisodesBy(season: selectedSeason)
    }
    
    @ViewBuilder
    func episodeCell(for episode: Episode) -> some View {
        VStack {
            Text("Episode \(episode.number)")
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, minHeight: 30)
                .padding()
                .background(Color.blue.opacity(0.7))
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(Color.blue, lineWidth: 2)
                )
        }
        .padding(.bottom, 8)
    }
}

struct EpisodeListView_Previews: PreviewProvider {
    static var previews: some View {
        EpisodeListView(selectedSeason: .constant(1))
            .environmentObject(MovieDetailViewModel())
    }
}
