//
//  FavoritesView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct FavoritesView: View {
    @State private var tvShows: [SimpleTvShow] = []

    var body: some View {
        Group {
            if tvShows.isEmpty {
                Text("Hello, favorites!")
            } else {
                ScrollView {
                    MovieList(tvShows: tvShows)
                }
                .toolbar {
                    ToolbarItem(placement: .topBarTrailing) {
                        Button {
                            FavoriteManager.shared.removeAllFavoritesTVShows()
                            tvShows = FavoriteManager.shared.getFavoriteTVShows()
                        } label: {
                            Image(systemName: "trash")
                        }
                        
                    }
                }
            }
        }
        .onAppear {
            tvShows = FavoriteManager.shared.getFavoriteTVShows()
        }
        .onChange(of: tvShows.count) { _, _ in
            tvShows = FavoriteManager.shared.getFavoriteTVShows()
        }
    }
}

#Preview {
    FavoritesView()
}
