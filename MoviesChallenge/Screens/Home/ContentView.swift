//
//  ContentView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct ContentView: View {
    @State private var loggedIn = true
    var body: some View {
        Group {
            if loggedIn {
                HomeView()
            } else {
                Text("No access")
            }
        }
    }
}

#Preview {
    ContentView()
}
