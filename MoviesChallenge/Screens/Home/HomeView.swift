//
//  HomeView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject private var tvShowModel = HomeViewModel()
    @State private var page: Int = 0

    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                moviesGrid
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarBackButtonHidden(true)
            .toolbar {
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        
                    } label: {
                        Text("MoviesApp")
                            .font(.headline)
                            .foregroundStyle(Color.white)
                    }
                }

                ToolbarItem(placement: .topBarTrailing) {
                    NavigationLink {
                        SearchView()
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .foregroundStyle(.white)
                    }
                }
                
                ToolbarItem(placement: .topBarTrailing) {
                    NavigationLink {
                        FavoritesView()
                    } label: {
                        Image(systemName: "star")
                            .foregroundStyle(.white)
                    }
                }
            }
            .onAppear {tvShowModel.loadTVListShows(page: page)}
            .refreshable {
                withAnimation {
                    tvShowModel.loadTVListShows(page: page)
                }
            }
        }
    }
    
    @ViewBuilder
    private var moviesGrid: some View {
        VStack {
            PagesButtonView(selectedId: $page, onComplete: { num in
                page = num
            })
            if tvShowModel.isLoading {
                LoadingView()
            } else {
                movieListContent
            }
        }
    }
    
   
    @ViewBuilder
    private var movieListContent: some View {
        ScrollView {
            MovieList(tvShows: tvShowModel.tvShowList)
        }
        .environmentObject(tvShowModel)
        .onChange(of: page, { _, newPage in
            tvShowModel.loadTVListShows(page: newPage)
        })
    }
}

struct MovieList: View {
    @State var tvShows: [SimpleTvShow]
    
    var body: some View {
        let imageWidth = UIScreen.main.bounds.width / 3.5
        let imageHeight = UIScreen.main.bounds.height / 5.5
        LazyVGrid(columns: Array(repeating: GridItem(.flexible()), count: 3), spacing: 2) {
            ForEach(tvShows) { tvShow in
                NavigationLink {
                    MovieDetailView(movieId: tvShow.id)
                        .environmentObject(MovieDetailViewModel())
                } label: {
                    VStack {
                        if let image = tvShow.image {
                            RemoteImage(urlString: image.medium)
                                .aspectRatio(contentMode: .fill)
                                .frame(width: imageWidth, height: imageHeight )
                                .clipped()
                                .cornerRadius(8)
                        }
                        Text(tvShow.name)
                            .foregroundStyle(.white)
                            .font(.system(size: 14, weight: .medium))
                            .frame(maxWidth: .infinity, minHeight: 50, alignment: .top)
                            .lineLimit(2)
                    }
                }
            }
        }
    }
}

#Preview {
    HomeView()
}
