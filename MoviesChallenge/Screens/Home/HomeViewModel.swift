//
//  HomeViewModel.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class HomeViewModel: ObservableObject {
    @Published var tvShowList: [SimpleTvShow] = []
    @Published var isLoading: Bool = false
    
    private let tvShowListAPI = TVShowListAPI()
    
    func loadTVListShows(page num: Int) {
        isLoading = true
        
        tvShowListAPI.fetchTVShowList(page: num) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let tvShowList):
                    self?.tvShowList = tvShowList
                case .failure(let error):
                    print("Error fetching TV shows: \(error.localizedDescription)")
                }
            }
        }
    }
}
