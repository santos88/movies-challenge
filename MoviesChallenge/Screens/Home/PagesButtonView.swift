//
//  PagesButtonView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct PagesButtonView: View {
    @ObservedObject private var tvShowModel = HomeViewModel()
    @Binding var selectedId: Int
    let maxPage = 380
    var onComplete: ((_ num: Int) -> Void)
    
    var body: some View {
        ScrollViewReader { proxy in
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack {
                    ForEach(0..<maxPage, id: \.self) { num in
                        Button(action: {
                            withAnimation {
                                selectedId = num
                                onComplete(num)
                            }
                        }) {
                            Text("\(num + 1)")
                                .font(.headline)
                                .padding(.vertical, 8)
                                .padding(.horizontal, 14)
                                .background(selectedId == num ? Color.blue : Color.gray.opacity(0.5))
                                .foregroundColor(selectedId == num ? .white : .black)
                                .cornerRadius(20)
                        }
                        .disabled(selectedId == num)
                        .id(num)
                    }
                }
                .padding(.leading)
            }
            .frame(height: 50)
        }
    }
}

struct PagesButtonView_Previews: PreviewProvider {
    struct PreviewWrapper: View {
        @State private var selectedId: Int = 0
        
        var body: some View {
            PagesButtonView(selectedId: $selectedId, onComplete: { num in
                print("Selected: \(num + 1)")
            })
            .environmentObject(HomeViewModel())
        }
    }
    
    static var previews: some View {
        PreviewWrapper()
    }
}
