//
//  MovieDetailView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct MovieDetailView: View {
    @EnvironmentObject private var tvShowModel: MovieDetailViewModel
    @State var movieId: Int
    @State private var isSaved = false
    @State private var season: Int = 1
    
    var body: some View {
        VStack {
            Group {
                if tvShowModel.isLoading {
                    LoadingView()
                } else {
                    if let tvShowDetail = tvShowModel.tvShowDetail  {
                        DetailBody(tvShowDetail: tvShowDetail, seasons: $season)
                            .environmentObject(tvShowModel)
                    }
                }
            }
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text(tvShowModel.tvShowDetail?.name ?? "")
                    .font(.headline)
            }
            ToolbarItem(placement: .topBarTrailing) {
                Button {
                    if let tvShowDetail = tvShowModel.tvShowDetail {
                        if isSaved {
                            FavoriteManager.shared.removeFromFavoritesTVShows(tvShow: SimpleTvShow(tvShow: tvShowDetail))
                            isSaved = false
                        } else {
                            FavoriteManager.shared.addNewFavoriteTVShow(tvShow: SimpleTvShow(tvShow: tvShowDetail))
                            isSaved = true
                        }
                    }
                } label: {
                    Image(systemName: isSaved ? "star.fill" : "star")
                        .foregroundStyle(.white)
                }
                
            }
        }
        .task {
            tvShowModel.loadTVShows(id: movieId)
            let tvShows = FavoriteManager.shared.getFavoriteTVShows()
            for tvShow in tvShows {
                if tvShow.id == movieId {
                    isSaved = true
                    break
                }
            }
        }
    }
}

struct DetailBody: View {
    @EnvironmentObject private var tvShowModel: MovieDetailViewModel
    @State var tvShowDetail: TvShow
    @Binding var seasons: Int
    
    var body: some View {
        ScrollView {
            VStack {
                VStack(spacing: 8) {
                    HStack(alignment: .center, spacing: 25) {
                        if let image = tvShowDetail.image {
                            RemoteImage(urlString: image.medium)
                                .frame(width: 160)
                                .clipped()
                                .cornerRadius(8)
                        } else {
                            Text("No poster")
                                .frame(width: 160)
                        }
                        VStack(alignment: .leading, spacing: 4) {
                            ScheduleView(schedule: tvShowDetail.schedule)
                                .padding(.bottom)
                            GenresView(genres: tvShowDetail.genres)
                                .padding(.bottom)
                        }
                    }
                    .padding(.horizontal)
                    .frame(maxWidth: .infinity,alignment: .leading)
                    SummaryView(summary: tvShowDetail.summary)
                        .padding(.horizontal)
                    SeasonPicker(selectedSeason: $seasons)
                        .padding(.bottom, 24)
                        .environmentObject(tvShowModel)
                }
            }
        }
    }
}

struct SummaryView: View {
    var summary: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("Summary")
                .font(.title2)
                .fontWeight(.bold)
            Text(summary.removingHTMLTags())
                .font(.body)
        }
        .padding(.vertical)
    }
}

struct GenresView: View {
    @State var genres: [String]
    var body: some View {
        VStack(alignment: .leading) {
            Text(genres.count > 1 ? "Genres:" : "Genre:")
                .font(.headline)
            VStack(alignment: .leading) {
                ForEach(genres, id: \.self) { genre in
                    Text("• \(genre)")
                        .padding(.leading)
                }
            }
        }
    }
}

struct ScheduleView: View {
    @State var schedule: Schedule
    var body: some View {
        VStack(alignment: .leading) {
            Text("Schedule:")
                .font(.headline)
            Text("Days: " + schedule.days.joined(separator: ", ") + ".")
                .padding(.leading)
            Text("Time: \(schedule.time)")
                .padding(.leading)
        }
    }
}

struct SeasonPicker: View {
    @EnvironmentObject private var tvShowModel: MovieDetailViewModel
    @Binding var selectedSeason: Int
    
    var body: some View {
        VStack {
            Picker("Season", selection: $selectedSeason) {
                ForEach(1..<tvShowModel.seasons+1, id: \.self) { season in
                    Text("Season \(season)")
                        .font(.headline)
                        .foregroundStyle(.white)
                }
            }
            .font(.title)
            .foregroundStyle(.white)
            .pickerStyle(.menu)
            
            EpisodeListView(selectedSeason: $selectedSeason)
                .environmentObject(tvShowModel)
        }
        .padding()
    }
}

struct RemoteImage: View {
    @State var urlString = ""
    
    var body: some View {
        Group {
            if let url = URL(string: urlString) {
                AsyncImage(url: url) { image in
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                } placeholder: {
                    ProgressView()
                }
            } else {
                Image(systemName: "rectangle.portrait.slash")
                    .foregroundStyle(.white)
            }
        }
    }
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let movieDetailViewModel = MovieDetailViewModel()
        
        MovieDetailView(movieId: 1)
            .environmentObject(movieDetailViewModel)
    }
}

extension String {
    func removingHTMLTags() -> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}
