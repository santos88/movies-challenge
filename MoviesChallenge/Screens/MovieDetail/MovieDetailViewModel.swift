//
//  MovieDetailViewModel.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation

class MovieDetailViewModel: ObservableObject {
    @Published var tvShowDetail: TvShow?
    @Published var seasons: Int = 0
    @Published var isLoading: Bool = false
    
    func loadTVShows(id: Int) {
        isLoading = true
        MovieDetailAPI().movieDetail(id: id) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
            }
            switch result {
            case .success(let tvShow):
                DispatchQueue.main.async {
                    self?.tvShowDetail = tvShow
                    self?.getSeasons(tvShowDetail: tvShow)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func getSeasons(tvShowDetail: TvShow) {
        guard let embedded = tvShowDetail.embedded else { return }
        let allSeasons = embedded.episodes.map { $0.season }
        seasons = Set(allSeasons).count
    }
    
    func getEpisodesBy(season: Int) -> [Episode] {
        guard let tvShowDetail = tvShowDetail,
              let embedded = tvShowDetail.embedded
        else { return [] }        
        let episodes = embedded.episodes.filter { $0.season == season }
        return episodes
    }
}
