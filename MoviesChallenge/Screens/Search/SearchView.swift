//
//  SearchView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject var searchViewModel = SearchViewModel()
    @FocusState private var isFocused: Bool

    var body: some View {
        VStack {
            if searchViewModel.isLoading {
                LoadingView()
                    .progressViewStyle(CircularProgressViewStyle())
            } else if searchViewModel.tvShowResults.isEmpty {
                Text("No results found")
                    .padding()
            } else {
                ScrollView {
                    MovieSearchedList(tvShows: searchViewModel.tvShowResults)
                }
            }
        }
        .navigationTitle("Search")
        .searchable(text: $searchViewModel.searchString)
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.isFocused = true
            }
        }
    }
}

struct MovieSearchedList: View {
    @State var tvShows: [TvShowSearched]
    let imageWidth = UIScreen.main.bounds.width / 3.5
    let imageHeight = UIScreen.main.bounds.height / 5.5
    
    var body: some View {
        LazyVGrid(columns: Array(repeating: GridItem(.flexible()), count: 3), spacing: 2) {
            ForEach(tvShows) { tvShow in
                NavigationLink {
                    MovieDetailView(movieId: tvShow.show.id)
                        .environmentObject(MovieDetailViewModel())
                } label: {
                    VStack {
                        if let image = tvShow.show.image {
                            RemoteImage(urlString: image.medium)
                                .aspectRatio(contentMode: .fill)
                                .frame(width:  imageWidth, height: imageHeight)
                                .clipped()
                                .cornerRadius(8)
                            
                            Text(tvShow.show.name)
                                .foregroundStyle(.white)
                                .font(.system(size: 14, weight: .medium))
                                .frame(maxWidth: .infinity, minHeight: 50, alignment: .top)
                                .lineLimit(2)
                        } else {
                            Text(tvShow.show.name)
                                .foregroundStyle(.white)
                                .font(.system(size: 14, weight: .medium))
                                .frame(maxWidth: .infinity, minHeight: 50, alignment: .top)
                                .lineLimit(2)
                        }
                        
                    }
                }
            }
        }
        .padding([.horizontal, .top])
        .edgesIgnoringSafeArea(.all)
    }
}

#Preview {
    SearchView()
}
