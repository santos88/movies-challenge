//
//  SearchViewModel.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import Foundation
import Combine

class SearchViewModel: ObservableObject {
    @Published var searchString: String = ""
    @Published var tvShowResults: [TvShowSearched] = []
    @Published var isLoading: Bool = false
    
    private var searchCancellable: AnyCancellable?
    private let tvShowSearchAPI = TVShowSearchAPI()
    
    init() {
        searchCancellable = $searchString
            .debounce(for: .milliseconds(500), scheduler: DispatchQueue.main)
            .sink { [weak self] searchString in
                self?.loadTVShowsResults(movie: searchString)
            }
    }
    
    func loadTVShowsResults(movie: String) {
        guard !movie.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            DispatchQueue.main.async {
                self.tvShowResults = []
                self.isLoading = false
            }
            return
        }
        
        isLoading = true
        
        tvShowSearchAPI.searchTVShows(query: movie) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let searchResults):
                    self?.tvShowResults = searchResults
                case .failure(let error):
                    print("Error searching for TV shows: \(error.localizedDescription)")
                    self?.tvShowResults = []
                }
            }
        }
    }
}
