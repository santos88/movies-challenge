//
//  LoadingView.swift
//  MoviesChallenge
//
//  Created by Santos Ramon on 4/5/24.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        ProgressView()
            .padding()
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
    }
}

#Preview {
    LoadingView()
}
